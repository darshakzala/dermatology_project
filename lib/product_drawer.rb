class ProductDrawer
  def self.draw(appointments)
    pdf = PDF::Writer.new
    appointments.each do |appointment|
      pdf.text appointment.physician.name
    end
    pdf.render
  end
end