class RemoveCodeDescriptionFromDiagnosticCode < ActiveRecord::Migration
  def change
    remove_column :diagnostic_codes, :codedescription
  end
end
