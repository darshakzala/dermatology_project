class AddAppointmentIdFromInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :appointment_id, :integer
  end
end
