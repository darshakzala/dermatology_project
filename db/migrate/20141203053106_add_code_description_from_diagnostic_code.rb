class AddCodeDescriptionFromDiagnosticCode < ActiveRecord::Migration
  def change
    add_column :diagnostic_codes, :codedescription, :string
  end
end
