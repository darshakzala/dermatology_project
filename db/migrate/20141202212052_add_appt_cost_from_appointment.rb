class AddApptCostFromAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :appointmentcost, :decimal
  end
end
