class RemoveAppointmentDateFromInvoices < ActiveRecord::Migration
  def change
    remove_column :invoices, :appointment_date
  end
end
