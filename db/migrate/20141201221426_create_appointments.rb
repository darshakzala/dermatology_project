class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.text :reason
      t.datetime :appointment_date
      t.integer :diagnostic_code_id

      t.timestamps null: false
    end
  end
end
