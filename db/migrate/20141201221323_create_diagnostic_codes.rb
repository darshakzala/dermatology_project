class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.string :Code_Num
      t.text :Code_Description

      t.timestamps null: false
    end
  end
end
