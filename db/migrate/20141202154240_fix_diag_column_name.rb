class FixDiagColumnName < ActiveRecord::Migration
  def change
    rename_column :diagnostic_codes, :Code_Num, :codenum
    rename_column :diagnostic_codes, :Code_Description, :codedescription
  end
end
