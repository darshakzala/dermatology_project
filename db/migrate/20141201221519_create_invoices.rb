class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.datetime :appointment_date
      t.integer :patient_id
      t.integer :physician_id
      t.integer :diagnostic_code_id
      t.decimal :appointment_cost

      t.timestamps null: false
    end
  end
end
