class PhysicianapptController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update]

  helper_method :physicianappointment
  def physicianappointment
    @appointments = Appointment.all
  end

  def edit

  end

  def update
    respond_to do |format|
      if @appointment.update(appointment_params)
        format.html { redirect_to @appointment, notice: 'Appointment was successfully updated.' }
        format.json { render :show, status: :ok, location: @appointment }
      else
        format.html { render :edit }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_appointment
    @appointment = Appointment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def appointment_params
    params.require(:appointment).permit(:patient_id, :physician_id, :reason, :appointment_date, :diagnostic_code_id)
  end
end
