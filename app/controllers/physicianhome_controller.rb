class PhysicianhomeController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update]

  helper_method :physicianhome
  def physicianhome
    @appointments = Appointment.all
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_appointment
    @appointment = Appointment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def appointment_params
    params.require(:appointment).permit(:patient_id, :physician_id, :reason, :appointment_date)
  end
end
