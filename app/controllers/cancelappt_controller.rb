class CancelapptController < ApplicationController
  before_action :set_appointment, only: [:destroy]

  helper_method :cancelappointment
  def cancelappointment
    @appointments = Appointment.all
  end

  def destroy
    @appointment.destroy
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_appointment
    @appointment = Appointment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def appointment_params
    params.require(:appointment).permit(:patient_id, :physician_id, :reason, :appointment_date, :diagnostic_code_id)
  end
end
