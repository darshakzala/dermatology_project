class PhysicianPDF < Prawn::Document
  def initialize(appointment)
    super()
    @appointment = appointment
    physician_info
  end

  def physician_info
    text "#{@appointment.physician.name}"
  end


end