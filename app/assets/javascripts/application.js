// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

//= require bootstrap-datetimepicker

$(function() {
   $("#appointment_appointment_date").datetimepicker({
       showOn: 'button',
       buttonImage: "assets/images/calendar.png",
       buttonImageOnly: true,
       buttonText: "Select a date",
       format: "MM dd yyyy - HH:ii P",
       daysOfWeekDisabled: [0,6],
       minuteStep: 30,
       showMeridian: true,
       hoursDisabled: [0,1,2,3,4,5,6,7,17,18,19,20,21,22,23]
   });
});