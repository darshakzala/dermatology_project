class DiagnosticCode < ActiveRecord::Base
  has_many :appointments
  has_many :invoices

end
