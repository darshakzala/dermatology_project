class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :physician
  belongs_to :diagnostic_code
  has_one :invoice

  validates_uniqueness_of :appointment_date, scope: :physician

end
