class Invoice < ActiveRecord::Base
  belongs_to :patient
  belongs_to :physician
  belongs_to :diagnostic_code
  has_one :appointment
end
