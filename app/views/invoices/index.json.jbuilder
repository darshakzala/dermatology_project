json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :appointment_date, :patient_id, :physician_id, :diagnostic_code_id, :appointment_cost
  json.url invoice_url(invoice, format: :json)
end
