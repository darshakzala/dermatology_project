json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :physician_id, :reason, :appointment_date, :diagnostic_code_id
  json.url appointment_url(appointment, format: :json)
end
