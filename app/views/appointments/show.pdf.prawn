pdf.text "Physician #{@appointment.physician.name}", :size => 30, :style => :bold

pdf.move_down(30)

pdf.text #{@appointment.appointment_date}
pdf.text #{number_to_currency(@appointment.appointmentcost)}

pdf.move_down(10)